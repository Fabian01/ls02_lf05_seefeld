package KriegDerRaumschiffe;

public class Ladung {
	private String bezeichnung;
	private int menge;
	
	
		public Ladung() {
			
		}
		
		public Ladung(String bezeichnung, int menge) {
			setBezeichnung(bezeichnung);
			setMenge(menge);
		}
			public int getMenge() {
				return menge;
			}
				private void setMenge(int menge) {
					this.menge = menge;
			}

				public String getBezeichnung() {
					return bezeichnung;
				}
					private void setBezeichnung(String bezeichnung) {
						this.bezeichnung = bezeichnung;
					}
}
