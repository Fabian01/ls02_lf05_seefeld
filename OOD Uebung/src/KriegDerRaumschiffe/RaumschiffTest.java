package KriegDerRaumschiffe;
import java.util.ArrayList;
public class RaumschiffTest {


	public static void main(String[] args) {
		Raumschiff k = new Raumschiff ("IKS Hengh'ta", 100, 100, 100, 100, 2, 1);
        Raumschiff r = new Raumschiff ("IRW Khazara", 100, 100, 100, 100, 2,2);
        Raumschiff v = new Raumschiff ("Ni Var", 80, 80, 50, 100, 5, 0);

        Ladung ladungK1 = new Ladung ("Ferengi, Schneckensaft", 200);
        Ladung ladungK2 = new Ladung ("Bat'leth Klingonen Schwert", 200);
        Ladung ladungR1 = new Ladung ("Borg-Schrott", 5);
        Ladung ladungR2 = new Ladung ("Rote Materie", 2);
        Ladung ladungR3 = new Ladung ("Plasma-Waffe", 50);
        Ladung ladungV1 = new Ladung ("Forschungssonde", 35);
        Ladung ladungV2 = new Ladung ("Photonentorpedo", 3);

        k.addLadung(ladungK1);
        k.addLadung(ladungK2);
        r.addLadung(ladungR1);
        r.addLadung(ladungR2);
        r.addLadung(ladungR3);
        v.addLadung(ladungV1);
        v.addLadung(ladungV2);
        
        
        k.photonentorpedoSchiessen (r);
        r.phaserkanonenSchiessen(k);

        v.nachrichtAnAlle("Gewalt ist nicht logisch.");

        k.zustandRaumschiff();
        k.LadungsverzeichnisAusgeben();

        k.photonentorpedoSchiessen(r);
        k.photonentorpedoSchiessen(r);

        k.zustandRaumschiff();
        k.ladungsverzeichnisAusgeben();

        r.zustandRaumschiff();
        r.ladungsverzeichnisAusgeben();

        v.zustandRaumschiff();
        v.ladungsverzeichnisAusgeben();



    }​​​​

}

}
