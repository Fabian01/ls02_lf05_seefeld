package KriegDerRaumschiffe;
import java.util.ArrayList;
public class Raumschiff {
		
	private String name;
	private int energieversorgunginProzent;
	private ArrayList<String> logbuchEintraege = new ArrayList<String>();
	private int trefferAnzahl;
	private int lebenserhaltungssystemeinProzent;
	private int schildeinProzent;
	private int huelleinProzent;
	private int photonentorpedoesAnzahl;
	private int androidenAnzahl;
	private int Tricoder;
	private int Forschungssonden;
	private int Phaserkanonen;
	private ArrayList<String> broadcastKommunikator = new ArrayList<String>();
	private ArrayList<Ladung> Ladungsverzeichnis = new ArrayList<Ladung>();
	
	public Raumschiff() {
		
	}

	public Raumschiff(String name, int energieversorgunginProzent,int Photonentorpedoes, int statusLebenserhaltungsSysteme, int huelleinProzent, int androidenAnzahl, int schildeinProzent) {
		setName(name);
		setEnergieversorgung(energieversorgunginProzent);
		setPhotonentorpedoes(Photonentorpedoes);
		setLebenserhaltungsSystemeInProzent(statusLebenserhaltungsSysteme);
		setHuelleinProzent(huelleinProzent);
		setSchildeInProzent(schildeinProzent);
		setAndroidenAnzahl(androidenAnzahl);
	}
	
	public int getSchildeInProzent() {
		return schildeinProzent;
	}
	public void setSchildeInProzent(int schildeinProzent) {
		this.schildeinProzent = schildeinProzent;
	}

	public int getAndroidenAnzahl() {
		return androidenAnzahl;
	}
	public void setAndroidenAnzahl(int androidenAnzahl) {
		this.androidenAnzahl = androidenAnzahl;
	}

	public int getHuelleinProzent(){
		return huelleinProzent;
	}
	public void setHuelleinProzent(int huelleinProzent) {
		this.huelleinProzent = huelleinProzent;
		
	}

	public int getLebenserhaltungsSystemeInProzent() {
		return lebenserhaltungssystemeinProzent;
	}
	public void setLebenserhaltungsSystemeInProzent(int LebenserhaltungsSystemeInProzent) {
		this.lebenserhaltungssystemeinProzent = LebenserhaltungsSystemeInProzent;
	}

	public int getPhotonentorpedoes() {
		return photonentorpedoesAnzahl;
	}
	public void setPhotonentorpedoes(int photonentorpedoes) {
		this.photonentorpedoesAnzahl = photonentorpedoes;
		
	}

	public ArrayList<String> getLogbuchEinträge() {
		return logbuchEintraege;
	}
	public void setLogbuchEinträge(ArrayList<String> logbuchEintraege) {
		this.logbuchEintraege = logbuchEintraege;
		
	}

	public int getEnergieversorgung() {
		return energieversorgunginProzent;
	}
	public void setEnergieversorgung(int energieVersorgung) {
		this.energieversorgunginProzent = energieVersorgung;
		
	}

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name  = name;
	}

	public void addLadung(Ladung ladung) {
		
	}

	
	
}
