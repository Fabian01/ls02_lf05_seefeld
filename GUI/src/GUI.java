import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.BorderLayout;
import java.awt.Color;

import javax.swing.SwingConstants;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import java.awt.Font;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JColorChooser;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class GUI {

	private JFrame frame;
	private Object contentPane;
	private JTextField txtHierBitteText;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					GUI window = new GUI();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public GUI() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	public void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 375, 650);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblText = new JLabel("Dieser Text soll ver\u00E4ndert werden",SwingConstants.CENTER);
		lblText.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblText.setBounds(5, 39, 343, 31);
		frame.getContentPane().add(lblText);
		
		
		
		JLabel lblNewLabel_1 = new JLabel("Aufgabe 1: Hintergrundfarbe \u00E4ndern");
		lblNewLabel_1.setFont(new Font("Tahoma", Font.BOLD, 13));
		lblNewLabel_1.setBounds(5, 107, 245, 21);
		frame.getContentPane().add(lblNewLabel_1);
		
		JButton btnRot = new JButton("Rot");
		btnRot.setBounds(5, 138, 108, 21);
		btnRot.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent arg0) {
				buttonRot_clicked();
			}
		});
		frame.getContentPane().add(btnRot);
		
		JButton btnBlau = new JButton("Blau");
		btnBlau.setBounds(121, 138, 108, 21);
		btnBlau.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent arg0) {
				buttonBlau_clicked();
			}
		});
		frame.getContentPane().add(btnBlau);
		
		JButton btnGrün = new JButton("Gr\u00FCn");
		btnGrün.setBounds(237, 138, 108, 21);
		btnGrün.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent arg0) {
				buttonGrün_clicked();
			}
		});
		frame.getContentPane().add(btnGrün);
		
		JButton btnGelb = new JButton("Gelb");
		btnGelb.setBounds(5, 169, 108, 21);
		btnGelb.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent arg0) {
				buttonGelb_clicked();
			}
		});
		frame.getContentPane().add(btnGelb);
		
		JButton btnReset = new JButton("Reset");
		btnReset.setBounds(121, 169, 108, 21);
		btnReset.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent arg0) {
				buttonReset_clicked();
			}
		});
		frame.getContentPane().add(btnReset);
		
		JButton btnAuwahl = new JButton("Ausw\u00E4hlen:");
		JColorChooser colorChooser = new JColorChooser(Color.black);
		btnAuwahl.setBounds(237, 169, 108, 19);
		btnAuwahl.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent arg0) {
				buttonAuswahl_clicked();
			}
		});
		frame.getContentPane().add(btnAuwahl);
		
		JLabel lblNewLabel_2 = new JLabel("Aufgabe 2: Text formatieren");
		lblNewLabel_2.setFont(new Font("Tahoma", Font.BOLD, 13));
		lblNewLabel_2.setBounds(5, 194, 227, 19);
		frame.getContentPane().add(lblNewLabel_2);
		
		JButton btnArial = new JButton("Arial");
		
		btnArial.setBounds(5, 217, 108, 21);
		btnArial.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent arg0) {
				lblText.setFont(new Font("Arial",Font.PLAIN,15));
			}
		});
		frame.getContentPane().add(btnArial);
		
		JButton btnComicSans = new JButton("Comic Sans");
		btnComicSans.setBounds(121, 217, 108, 21);
		btnComicSans.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent arg0) {
				lblText.setFont(new Font("Comic Sans MS",Font.PLAIN,15));
			}
		});
		frame.getContentPane().add(btnComicSans);
		
		JButton btnCourierNew = new JButton("Courier New");
		btnCourierNew.setBounds(237, 217, 108, 21);
		btnCourierNew.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent arg0) {
				lblText.setFont(new Font("Courier New",Font.PLAIN,15));
			}
		});
		frame.getContentPane().add(btnCourierNew);
		
		txtHierBitteText = new JTextField();
		txtHierBitteText.setText("Hier bitte Text eingeben");
		txtHierBitteText.setFont(new Font("Arial",Font.PLAIN,13));
		txtHierBitteText.setBounds(5, 248, 340, 20);
		frame.getContentPane().add(txtHierBitteText);
		txtHierBitteText.setColumns(10);
		
		JButton btnLabelText = new JButton("Ins Label schreiben");
		btnLabelText.setBounds(5, 278, 171, 21);
		btnLabelText.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent arg0) {
				lblText.setText(txtHierBitteText.getText());
			}
		});
		frame.getContentPane().add(btnLabelText);
		
		JButton btnTextLöschen = new JButton("Text l\u00F6schen");
		btnTextLöschen.setBounds(180, 278, 171, 21);
		btnTextLöschen.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent arg0) {
				lblText.setText(null);
			}
		});
		frame.getContentPane().add(btnTextLöschen);
		
		JLabel lblNewLabel_2_1 = new JLabel("Aufgabe 3: Schriftfarbe \u00E4ndern");
		lblNewLabel_2_1.setFont(new Font("Tahoma", Font.BOLD, 13));
		lblNewLabel_2_1.setBounds(2, 314, 227, 19);
		frame.getContentPane().add(lblNewLabel_2_1);
		
		JButton btnSchriftfarbeRot = new JButton("Rot");
		btnSchriftfarbeRot.setBounds(5, 343, 108, 21);
		btnSchriftfarbeRot.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent arg0) {
				lblText.setForeground(Color.red);
			}
		});
		frame.getContentPane().add(btnSchriftfarbeRot);
		
		JButton btnSchriftfarbeBlau = new JButton("Blau");
		btnSchriftfarbeBlau.setBounds(121, 343, 108, 21);
		btnSchriftfarbeBlau.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent arg0) {
				lblText.setForeground(Color.blue);
			}
		});
		frame.getContentPane().add(btnSchriftfarbeBlau);
		
		JButton btnSchriftfarbeSchwarz = new JButton("Schwarz");
		btnSchriftfarbeSchwarz.setBounds(237, 343, 108, 21);
		btnSchriftfarbeSchwarz.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent arg0) {
				lblText.setForeground(Color.black);
			}
		});
		frame.getContentPane().add(btnSchriftfarbeSchwarz);
		
		JLabel lblAufgabe4 = new JLabel("Aufgabe 4: Schriftgr\u00F6\u00DFe \u00E4ndern");
		lblAufgabe4.setFont(new Font("Tahoma", Font.BOLD, 13));
		lblAufgabe4.setBounds(2, 386, 227, 19);
		frame.getContentPane().add(lblAufgabe4);
		
		JButton btnGrößer = new JButton("+");
		btnGrößer.setBounds(2, 415, 171, 21);
		btnGrößer.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent arg0) {
				int groesse = lblText.getFont().getSize();
				lblText.setFont(new Font("Arial", Font.PLAIN, groesse + 1));
			}
		});
		frame.getContentPane().add(btnGrößer);
		
		JButton btnKleiner = new JButton("-");
		btnKleiner.setBounds(180, 415, 171, 21);
		btnKleiner.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent arg0) {
				int groesse = lblText.getFont().getSize();
				lblText.setFont(new Font("Arial", Font.PLAIN, groesse - 1));
			}
		});
		frame.getContentPane().add(btnKleiner);
		
		JLabel lblAufgabe5 = new JLabel("Aufgabe 5: Textausrichtung");
		lblAufgabe5.setFont(new Font("Tahoma", Font.BOLD, 13));
		lblAufgabe5.setBounds(2, 450, 227, 19);
		frame.getContentPane().add(lblAufgabe5);
		
		JButton btnlinksbündig = new JButton("linksb\u00FCndig");
		btnlinksbündig.setBounds(5, 479, 108, 21);
		btnlinksbündig.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent arg0) {
				lblText.setHorizontalAlignment(SwingConstants.LEFT);
			}
		});
		frame.getContentPane().add(btnlinksbündig);
		
		JButton btnzentriert = new JButton("zentriert");
		btnzentriert.setBounds(121, 479, 108, 21);
		btnzentriert.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent arg0) {
				lblText.setHorizontalAlignment(SwingConstants.CENTER);
			}
		});
		frame.getContentPane().add(btnzentriert);
		
		JButton btnrechtsbündig = new JButton("rechtsb\u00FCndig");
		btnrechtsbündig.setBounds(237, 479, 108, 21);
		btnrechtsbündig.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent arg0) {
				lblText.setHorizontalAlignment(SwingConstants.RIGHT);
			}
		});
		frame.getContentPane().add(btnrechtsbündig);
		
		JButton btnExit = new JButton("EXIT");
		btnExit.setFont(new Font("Tahoma", Font.PLAIN, 17));
		btnExit.setBounds(5, 510, 346, 91);
		btnExit.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent arg0) {
				System.exit(1);
			}
		});
		frame.getContentPane().add(btnExit);
		
}

	public void buttonAuswahl_clicked() {
		Color initialBackground = frame.getBackground();
		Color background = JColorChooser.showDialog(null, "JColorChooser Sample", initialBackground);
		if (background !=null) {
			frame.getContentPane().setBackground(background);
		}
	}

	public void buttonReset_clicked() {
		this.frame.getContentPane().setBackground(null);
		
	}

	public void buttonGelb_clicked() {
		this.frame.getContentPane().setBackground(Color.YELLOW);
		
	}

	public void buttonGrün_clicked() {
		this.frame.getContentPane().setBackground(Color.green);
		
	}

	public void buttonRot_clicked() {
		this.frame.getContentPane().setBackground(Color.red);
		
	}
	public void buttonBlau_clicked() {
		this.frame.getContentPane().setBackground(Color.blue);
		
	}
}
